﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text distanceText;
    public GameObject menu;
    public Text text;

    void Start()
    {
        distanceText.text = GameManager.i.distance.ToString();
    }

    private void Update()
    {
        distanceText.text = Math.Round(GameManager.i.distance).ToString() + "m";
    }

    public void GameOverScreen()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GameManager.i.Restart();
        }
    }

    public void RestartButton()
    {
        GameManager.i.paused = false;
        GameManager.i.HUD.menu.SetActive(false);
        GameManager.i.Restart();
    }

    public void MenuButton()
    {
        
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}