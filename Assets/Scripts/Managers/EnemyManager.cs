﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager i;

    [Header("Instantiate Data")]
    [SerializeField] private float minTime = 2f;
    [SerializeField] private float maxTime = 5f;
    [SerializeField] public List<GameObject> enemies;
    [SerializeField] public List<int> spawnProbabilities;
    [NonSerialized] private float respawningTimer;
    [NonSerialized] public bool spawn = true;

    private void Awake()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (spawn)
        {
            ChangeDifficulty();
            SpawnEnemies();
        }
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            // Calcular el peso total de todos los enemigos
            int totalWeight = 0;
            for (int i = 0; i < spawnProbabilities.Count; i++)
            {
                totalWeight += spawnProbabilities[i];
            }

            // Generar un número aleatorio entre 0 y el peso total
            int randomWeight = UnityEngine.Random.Range(0, totalWeight);

            // Determinar qué enemigo spawnea basado en el número aleatorio y los pesos
            int cumulativeWeight = 0;
            GameObject selectedEnemy = null;
            for (int i = 0; i < enemies.Count; i++)
            {
                cumulativeWeight += spawnProbabilities[i];
                if (randomWeight < cumulativeWeight)
                {
                    selectedEnemy = enemies[i];
                    break;
                }
            }

            // Instanciar el enemigo seleccionado
            if (selectedEnemy != null)
            {
                Enemy newEnemy = Instantiate(selectedEnemy, transform.position, Quaternion.identity).GetComponent<Enemy>();
                newEnemy.transform.parent = GameManager.i.enemies.transform;
            }

            // Reiniciar el temporizador
            respawningTimer = UnityEngine.Random.Range(minTime, maxTime);
        }
    }

    public void ChangeDifficulty()
    {
        if (GameManager.i.distance >= 250)
        {
            minTime = 0.55f;
            maxTime = 0.60f;
        }
        else if (GameManager.i.distance >= 200)
        {
            spawnProbabilities[0] = 40;
            spawnProbabilities[1] = 35;
            spawnProbabilities[2] = 25;
        }
        else if (GameManager.i.distance >= 150)
        {
            minTime = 0.65f;
            maxTime = 0.85f;
        }
        else if (GameManager.i.distance >= 100)
        {
            spawnProbabilities[0] = 45;
            spawnProbabilities[1] = 25;
            spawnProbabilities[2] = 15;
        }
        else if (GameManager.i.distance >= 75)
        {
            minTime = 0.75f;
            maxTime = 0.85f;
        }
        else if (GameManager.i.distance >= 50)
        {
            spawnProbabilities[0] = 60;
            spawnProbabilities[1] = 40;
            spawnProbabilities[2] = 0;
        }
        else if (GameManager.i.distance >= 25)
        {
            minTime = 0.80f;
            maxTime = 0.85f;
        }
        else if (GameManager.i.distance >= 0)
        {
            minTime = 0.85f;
            maxTime = 1.00f;
            spawnProbabilities[0] = 100;
            spawnProbabilities[1] = 0;
            spawnProbabilities[2] = 0;
        }
    }
}
