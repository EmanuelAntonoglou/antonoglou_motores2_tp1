using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeEgg : MonoBehaviour
{
    private Enemy enemy;

    private void Start()
    {
        enemy = transform.parent.parent.GetComponent<Enemy>();
    }

    public void Explode()
    {
        if (enemy.gameObject.name == "Evil Wizard(Clone)")
        {
            enemy.uiSequence.DestroyAllChilds();
            enemy.sequence.Clear();

            if (enemy.sequence.Count == 0)
            {
                enemy.phase++;
                if (enemy.phase != 3)
                {
                    enemy.canMove = false;
                    enemy.col.enabled = true;
                    enemy.col.isTrigger = false;
                    enemy.animator.Play("Take Hit");
                    enemy.minSequence++;
                    enemy.maxSequence++;
                }
                else
                {
                    enemy.Invoke("DestroyEnemy", 0.075f);
                }
            }
        }
        else if (enemy.gameObject.name != "Wizard Goose(Clone)")
        {
            enemy.Invoke("DestroyEnemy", 0.075f);
        }
        Destroy(transform.parent.gameObject);
    }
}
