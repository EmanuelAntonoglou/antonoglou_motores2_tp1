using System;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

public class SequenceManager : MonoBehaviour
{
    public static SequenceManager i;

    public List<KeyCode> playerSequence = new List<KeyCode>();
    public UISequence playerUISequence;
    public bool inputEnabled = true;
    public List<Enemy> enemies = new List<Enemy>();

    [Header("Miss")]
    public bool missed = false;
    public int comboActual = 0;
    public int cantidadEnemigos = 0;
    public bool flagKeyPressed = false;

    private void Awake()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        playerUISequence = GameManager.i.player.gameObject.transform.Find("UISequence").GetComponent<UISequence>();
    }

    private void Update()
    {
        if (!GameManager.i.paused)
        {
            if (flagKeyPressed && cantidadEnemigos != 0 && comboActual == (cantidadEnemigos * -1))
            {
                inputEnabled = false;
                playerUISequence.CreateProhibited();
                if (!GameManager.i.beingKill)
                {
                    Invoke("EnableInput", 0.5f);
                }
                flagKeyPressed = false;
            }

            if (inputEnabled && Input.anyKeyDown)
            {
                KeyCode pressedKey = GetKeyCode();
                if (pressedKey != KeyCode.None)
                {
                    flagKeyPressed = true;
                    comboActual = 0;
                    playerUISequence.CreateUI(pressedKey);
                    playerUISequence.Invoke("DestroyFirstCommand", 0.1f);
                }
            }
        }
    }

    private KeyCode GetKeyCode()
    {
        foreach (KeyCode tecla in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(tecla) &&
               (tecla == KeyCode.UpArrow || tecla == KeyCode.DownArrow || tecla == KeyCode.LeftArrow || tecla == KeyCode.RightArrow))
            {
                return tecla;
            }
        }
        return KeyCode.None;
    }

    private void EnableInput()
    {
        inputEnabled = true;
        playerUISequence.DestroyAllChilds();
    }

    public void RemoveEnemy(Enemy enemy)
    {
        enemies.Remove(enemy);
        playerSequence.Clear();
    }
}