﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour 
{
	public float speed;
    public float originalSpeed;
	private float speedFactor = 1.25f;

    private float x;
	public float PontoDeDestino;
	public float PontoOriginal;

	void Start() 
	{
        originalSpeed = speed;
    }
	
	void Update() 
	{
		x = transform.position.x;
		x += speed * speedFactor * Time.deltaTime;
		transform.position = new Vector3 (x, transform.position.y, transform.position.z);

		if (x <= PontoDeDestino){

			x = PontoOriginal;
			transform.position = new Vector3 (x, transform.position.y, transform.position.z);
		}
	}

	public void PauseParallax()
	{
		speed = 0;
    }

    public void ContinueParallax()
    {
        speed = originalSpeed;
    }
}
