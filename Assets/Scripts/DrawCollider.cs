using UnityEngine;

public class DrawCollider : MonoBehaviour
{
    [SerializeField] public bool allowDifferentColors = false;
    [SerializeField] public Color borderColor = Color.white;
    [SerializeField] public Color fillColor = Color.yellow;

    private void OnValidate()
    {
        if (!allowDifferentColors)
        {
            fillColor = new Color(borderColor.r, borderColor.g, borderColor.b, 0.5f);
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Collider collider = GetComponent<Collider>();

        if (collider != null)
        {
            Color usedFillColor = allowDifferentColors ? fillColor : new Color(borderColor.r, borderColor.g, borderColor.b, 0.5f);

            if (collider is BoxCollider)
            {
                var boxCollider = collider as BoxCollider;
                DrawBoxCollider(boxCollider, usedFillColor, borderColor);
            }
            else if (collider is SphereCollider)
            {
                var sphereCollider = collider as SphereCollider;
                DrawSphereCollider(sphereCollider, usedFillColor, borderColor);
            }
            else if (collider is CapsuleCollider)
            {
                var capsuleCollider = collider as CapsuleCollider;
                DrawCapsuleCollider(capsuleCollider, usedFillColor, borderColor);
            }
        }
    }
#endif

    private void DrawBoxCollider(BoxCollider boxCollider, Color fillColor, Color borderColor)
    {
        Gizmos.matrix = Matrix4x4.TRS(boxCollider.transform.position, boxCollider.transform.rotation, boxCollider.transform.lossyScale);

        // Dibujar relleno
        Gizmos.color = fillColor;
        Gizmos.DrawCube(boxCollider.center, boxCollider.size);

        // Dibujar borde
        Gizmos.color = borderColor;
        Gizmos.DrawWireCube(boxCollider.center, boxCollider.size);
    }

    private void DrawSphereCollider(SphereCollider sphereCollider, Color fillColor, Color borderColor)
    {
        // Calculate the transformation matrix including position, rotation, and scale
        Matrix4x4 transformationMatrix = Matrix4x4.TRS(sphereCollider.transform.position, sphereCollider.transform.rotation, sphereCollider.transform.lossyScale);

        // Apply the transformation matrix
        Gizmos.matrix = transformationMatrix;

        // Dibujar relleno
        Gizmos.color = fillColor;
        Gizmos.DrawSphere(sphereCollider.center, sphereCollider.radius);

        // Dibujar borde
        Gizmos.color = borderColor;
        Gizmos.DrawWireSphere(sphereCollider.center, sphereCollider.radius);
    }

    private void DrawCapsuleCollider(CapsuleCollider capsuleCollider, Color fillColor, Color borderColor)
    {
        Gizmos.color = fillColor;

        // Get capsule data
        Vector3 center = capsuleCollider.center;
        float radius = capsuleCollider.radius;
        float height = capsuleCollider.height;
        int direction = capsuleCollider.direction;

        // Apply object's scale
        Vector3 scaledCenter = Vector3.Scale(center, capsuleCollider.transform.lossyScale);
        float scaledRadius = radius * Mathf.Max(capsuleCollider.transform.lossyScale.x, capsuleCollider.transform.lossyScale.z);
        float scaledHeight = height * capsuleCollider.transform.lossyScale.y;

        // Calculate capsule endpoints in world space
        Vector3 point1, point2;
        if (direction == 0) // X-Axis
        {
            point1 = capsuleCollider.transform.TransformPoint(scaledCenter + new Vector3(scaledHeight / 2f, 0f, 0f));
            point2 = capsuleCollider.transform.TransformPoint(scaledCenter - new Vector3(scaledHeight / 2f, 0f, 0f));
        }
        else if (direction == 1) // Y-Axis
        {
            float yShift = (scaledHeight / 2f - scaledRadius) * capsuleCollider.transform.lossyScale.y; // Adjuste del desplazamiento vertical
            point1 = capsuleCollider.transform.TransformPoint(scaledCenter + new Vector3(0f, yShift, 0f));
            point2 = capsuleCollider.transform.TransformPoint(scaledCenter - new Vector3(0f, yShift, 0f));
        }
        else // Z-Axis
        {
            point1 = capsuleCollider.transform.TransformPoint(scaledCenter + new Vector3(0f, 0f, scaledHeight / 2f + scaledRadius));
            point2 = capsuleCollider.transform.TransformPoint(scaledCenter - new Vector3(0f, 0f, scaledHeight / 2f + scaledRadius));
        }

        // Draw spheres
        Gizmos.DrawSphere(point1, scaledRadius);
        Gizmos.DrawSphere(point2, scaledRadius);

        // Draw cylinder body
        DrawCylinder(point1, point2, scaledRadius, fillColor);
    }

    private void DrawCylinder(Vector3 start, Vector3 end, float radius, Color color)
    {
        Gizmos.color = color;

        Vector3 up = (end - start).normalized * radius;
        Vector3 forward = Vector3.Slerp(up, -up, 0.5f);
        Vector3 right = Vector3.Cross(up, forward).normalized * radius;

        // Draw the two spheres
        Gizmos.DrawWireSphere(start, radius);
        Gizmos.DrawWireSphere(end, radius);

        // Draw the cylinder body
        float height = Vector3.Distance(start, end);
        const int segments = 36;
        for (int i = 0; i < segments; i++)
        {
            float angle = 360f / segments * i;
            Quaternion rotator = Quaternion.AngleAxis(angle, up);
            Vector3 p1 = start + rotator * right;
            Vector3 p2 = end + rotator * right;
            Gizmos.DrawLine(p1, p2);
        }
    }

}
