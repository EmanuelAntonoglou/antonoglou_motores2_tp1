using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetrics;

public class GameManager : MonoBehaviour
{
    [NonSerialized] public static GameManager i;
    [SerializeField] public Controller_Player player;
    [NonSerialized] public float distance = 0;

    [SerializeField] public Controller_Hud HUD;
    [NonSerialized] public bool paused = false;
    [NonSerialized] public bool beingKill = false;
    [NonSerialized] public float record = 0f;
    [NonSerialized] public bool newRecord = false;

    [Header("Wizard Goose")]
    [SerializeField] public GameObject wizardGoose;
    [SerializeField] public GameObject eggBomb;
    [NonSerialized] public bool flagWG = true;
    [NonSerialized] public float timeWizardGoose = 300.0f;
    [NonSerialized] public int minEnemyForWG = 1;
    [NonSerialized] private Enemy WGInstance = null;
    [NonSerialized] public bool canWGAttack = false;

    [Header("Parallax Effect")]
    [SerializeField] private GameObject background;
    [NonSerialized] private List<MoveBackground> backgroundLayers = new List<MoveBackground>();

    [Header("Enemies")]
    [SerializeField] public GameObject enemies;
    [SerializeField] private GameObject evilWizard;
    [NonSerialized] public bool flagBoss = true;

    [Header("Wave")]
    [SerializeField] private float waveDuration = 300;
    [NonSerialized] public bool gameOver = false;
    [NonSerialized] public int wave = 0;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        SetTimeWizardGoose();

        foreach (Transform child in background.transform)
        {
            foreach (Transform layer in child.transform)
            {
                MoveBackground moveBackground = layer.GetComponent<MoveBackground>();
                if (moveBackground != null)
                {
                    backgroundLayers.Add(moveBackground);
                }
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!HUD.menu.activeSelf && !beingKill)
            {
                HUD.text.color = new Color(189f / 255f, 137f / 255f, 40f / 255f);
                paused = true;
                HUD.text.text = "Paused";
                HUD.menu.SetActive(true);
                Time.timeScale = 0;
            }
            else if (!beingKill)
            {
                paused = false;
                HUD.text.text = "Paused";
                HUD.menu.SetActive(false);
                Time.timeScale = 1;
            }
        }
        if (!gameOver)
        {
            distance += Time.deltaTime * 4;
        }
        if (!paused)
        {
            if (canWGAttack && Input.GetKeyDown(KeyCode.Space))
            {
                Invoke("ClearEnemies", 0.1f);
            }
            if (!canWGAttack && flagWG && distance >= timeWizardGoose)
            {
                flagWG = false;
                SpawnWizardGoose();
            }
            else if (distance >= ((wave + 1) * waveDuration))
            {
                EnemyManager.i.spawn = false;

                if (SequenceManager.i.enemies.Count != 0)
                {
                    CheckIfListEmpty();
                }
                else if (flagBoss && SequenceManager.i.enemies.Count == 0)
                {
                    flagBoss = false;
                    Invoke("SpawnBoss", 1.75f);
                }
            }
        }
    }

    private void CheckIfListEmpty()
    {
        List<int> missingComponentsIndexes = new List<int>();

        for (int i = SequenceManager.i.enemies.Count - 1; i >= 0; i--)
        {
            if (SequenceManager.i.enemies[i] == null || SequenceManager.i.enemies[i].GetComponent<Enemy>() == null)
            {
                missingComponentsIndexes.Add(i);
            }
        }

        foreach (int index in missingComponentsIndexes)
        {
            SequenceManager.i.enemies.RemoveAt(index);
        }
    }

    private void SpawnBoss()
    {
        Enemy eWInstance = Instantiate(evilWizard, EnemyManager.i.transform.position, Quaternion.identity).GetComponent<Enemy>();
        eWInstance.transform.parent = enemies.transform;
    }

    public void SetTimeWizardGoose()
    {
        timeWizardGoose = UnityEngine.Random.Range((wave * waveDuration) + 25, (wave * waveDuration) + 100);
    }

    public void Restart()
    {
        foreach (Transform enemy in enemies.transform)
        {
            Destroy(enemy.gameObject);
        }

        SequenceManager.i.enemies.Clear();
        SequenceManager.i.playerSequence.Clear();
        distance = 0;

        newRecord = false;
        beingKill = false;
        flagWG = true;
        canWGAttack = false;
        flagBoss = true;
        EnemyManager.i.spawn = true;
        player.animator.speed = 1.0f;
        player.animator.Play("Run");
        SequenceManager.i.inputEnabled = true;
        ContinueBackground();
        gameOver = false;
        wave = 0;
        SetTimeWizardGoose();
        SequenceManager.i.comboActual = 0;
        SequenceManager.i.cantidadEnemigos = 0;

        Time.timeScale = 1;
    }

    private void SpawnWizardGoose()
    {
        WGInstance = Instantiate(wizardGoose, EnemyManager.i.transform.position, Quaternion.identity).GetComponent<Enemy>();
        WGInstance.transform.parent = enemies.transform;
        WGInstance.transform.position += new Vector3(-0.75f, 1.577f, 0);
    }

    public void StartWizardGooseAttack()
    {
        player.animator.Play("Attack 1");
        WGInstance.animator.Play("Idle");
        WGInstance.transform.position += new Vector3(-5.525f, -1.25f, 0);
        canWGAttack = true;
    }

    private void ClearEnemies()
    {
        EnemyManager.i.spawn = false;
        foreach (Enemy enemy in SequenceManager.i.enemies)
        {
            if (enemy != null)
            {
                enemy.canMove = false;
                enemy.col.enabled = false;
                enemy.animator.Play("Idle");
                enemy.SpawnEggBomb();
            }
        }
        Invoke("KillWizardGoose", 1.25f);
        canWGAttack = false;
    }

    private void KillWizardGoose()
    {
        Destroy(WGInstance.gameObject);
        SequenceManager.i.enemies.Clear();
        EnemyManager.i.spawn = true;
    }

    public void PauseWizardGoose()
    {
        if (WGInstance != null)
        {
            Animator[] animators = WGInstance.GetComponentsInChildren<Animator>();
            foreach (Animator animator in animators)
            {
                animator.enabled = false;
            }
        }
    }

    public void PauseBackground()
    {
        foreach (MoveBackground layer in backgroundLayers)
        {
            layer.PauseParallax();
        }
    }

    public void ContinueBackground()
    {
        foreach (MoveBackground layer in backgroundLayers)
        {
            layer.ContinueParallax();
        }
    }
}
