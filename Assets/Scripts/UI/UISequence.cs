using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISequence : MonoBehaviour
{
    [SerializeField] private Sprite arrow;
    [SerializeField] private Sprite cantSpell;
    [NonSerialized] public List<Image> images = new List<Image>();
    private Enemy enemy;
    public Canvas canvas;

    private void Awake()
    {
        enemy = transform.parent.GetComponent<Enemy>();
        canvas = transform.Find("Canvas").GetComponent<Canvas>();
    }

    public void CreateUI(KeyCode key)
    {
        GameObject arrowObject = new GameObject("ArrowSprite");
        arrowObject.transform.SetParent(canvas.transform);

        Image image = arrowObject.AddComponent<Image>();
        images.Add(image);
        image.sprite = arrow;
        arrowObject.GetComponent<RectTransform>().localScale = new Vector3(3.5f, 3.5f, 3.5f);
        
        if (key == KeyCode.UpArrow)
        {
            image.color = Color.blue;
        }
        else if (key == KeyCode.RightArrow)
        {
            image.transform.Rotate(Vector3.forward, 270f);
            image.color = Color.red;
        }
        else if (key == KeyCode.DownArrow)
        {
            image.transform.Rotate(Vector3.forward, 180f);
            image.color = Color.green;
        }
        if (key == KeyCode.LeftArrow)
        {
            image.transform.Rotate(Vector3.forward, 90f);
            image.color = Color.yellow;
        }
    }

    public void CreateProhibited()
    {
        GameObject prohibitedInstance = new GameObject("Prohibited");
        prohibitedInstance.transform.SetParent(canvas.transform);

        Image image = prohibitedInstance.AddComponent<Image>();
        images.Add(image);
        image.sprite = cantSpell;
        prohibitedInstance.GetComponent<RectTransform>().localScale = new Vector3(4.5f, 4.5f, 4.5f);
    }

    public void DestroyFirstCommand()
    {
        Transform[] children = new Transform[canvas.transform.childCount];
        
        for (int i = 0; i < canvas.transform.childCount; i++)
        {
            children[i] = canvas.transform.GetChild(i);
        }

        Destroy(children[0].gameObject);
    }

    public void DestroyAllChilds()
    {
        for (int i = 0; i < canvas.transform.childCount; i++)
        {
            DestroyFirstCommand();
        }
    }

}
