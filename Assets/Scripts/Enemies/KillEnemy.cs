using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Unity.IO.LowLevel.Unsafe.AsyncReadManagerMetrics;

public class KillEnemy : MonoBehaviour
{
    private float hueValue = 0f;
    private float hueIncrement = 0.35f;
    private Color initialColor;
    private float initialSaturation;
    private float initialValue;

    private void Start()
    {
        GameManager.i.newRecord = false;
        initialColor = GameManager.i.HUD.text.color;
        Color.RGBToHSV(initialColor, out float hue, out initialSaturation, out initialValue);
    }

    private void Update()
    {
        if (GameManager.i.newRecord)
        {
            hueValue += hueIncrement * Time.deltaTime;

            if (hueValue > 1f)
            {
                hueValue -= 1f;
            }

            Color newColor = Color.HSVToRGB(hueValue, initialSaturation, initialValue);
            GameManager.i.HUD.text.color = newColor;
        }
    }

    public void Die()
    {
        SequenceManager.i.RemoveEnemy(GetComponent<Enemy>());
        Destroy(transform.parent.gameObject);
    }

    public void DieBoss()
    {
        GameManager.i.wave++;
        GameManager.i.flagBoss = true;
        EnemyManager.i.spawn = true;
        GameManager.i.flagWG = true;
        GameManager.i.SetTimeWizardGoose();
        Die();
    }

    public void KillPlayer()
    {
        GameManager.i.player.spriteRenderer.sortingOrder = 5;
        GetComponent<SpriteRenderer>().sortingOrder = 6;
        GameManager.i.player.animator.speed = 0.25f;
        GameManager.i.player.animator.Play("Hit");
    }

    public void Move()
    {
        Enemy enemyC = transform.parent.GetComponent<Enemy>();
        enemyC.canMove = true;
        enemyC.col.isTrigger = true;
        enemyC.uiSequence.transform.localPosition = new Vector3(enemyC.animator.transform.localPosition.x - 0.1f, enemyC.animator.transform.localPosition.y + 1, 0);
        DestroyBuggedUI(enemyC);
        enemyC.NewPhase();
    }

    private void DestroyBuggedUI(Enemy enemyC)
    {
        Transform canvasTransform = enemyC.uiSequence.canvas.transform;
        if (canvasTransform != null)
        {
            Transform[] children = new Transform[canvasTransform.childCount];
            for (int i = 0; i < children.Length; i++)
            {
                children[i] = canvasTransform.GetChild(i);
                if (children[i] != null)
                {
                    Destroy(children[i].gameObject);
                }
            }
        }
    }

    public void PlayerDied()
    {
        if (GameManager.i.distance > GameManager.i.record)
        {
            GameManager.i.newRecord = true;
            GameManager.i.record = GameManager.i.distance;
            GameManager.i.HUD.text.text = "New Best!";
        }
        else
        {
            GameManager.i.newRecord = false;
            GameManager.i.HUD.text.color = initialColor;
            GameManager.i.HUD.text.text = "Try Again!";
        }
        GameManager.i.HUD.menu.SetActive(true);
    }
}