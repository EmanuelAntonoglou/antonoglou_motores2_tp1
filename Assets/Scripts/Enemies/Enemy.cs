using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.EventSystems.EventTrigger;

[System.Serializable]
public class Enemy : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float enemyVelocity;
    [NonSerialized] public bool canMove = true;

    [Header("Sequence")]
    [SerializeField] public int minSequence = 1;
    [SerializeField] public int maxSequence = 2;
    [NonSerialized] private int sequenceSize = 2;
    [NonSerialized] public bool killable = false;
    [SerializeField] public List<KeyCode> sequence = new List<KeyCode>();
    [NonSerialized] private bool contado = false;

    [Header("Wizard Goose")]
    [NonSerialized] private GameObject eggBombInstance = null;

    [Header("Evil Wizard")]
    [NonSerialized] public int phase = 0;

    [Header("Components")]
    [NonSerialized] public Animator animator;
    [NonSerialized] public UISequence uiSequence;
    [NonSerialized] public Collider col;
    [NonSerialized] private Rigidbody rb;


    private void Awake()
    {
        uiSequence = transform.Find("UISequence").GetComponent<UISequence>();
        rb = GetComponent<Rigidbody>();
        animator = transform.Find("Sprite").GetComponent<Animator>();
        col = transform.Find("Sprite").GetComponent<Collider>();
        if (gameObject.name != "Evil Wizard")
        {
            SequenceManager.i.enemies.Add(this);
        }
    }

    private void Start()
    {
        SetRandomSequence();
        foreach (KeyCode key in sequence)
        {
            uiSequence.CreateUI(key);
        }
    }

    private void FixedUpdate()
    {
        if (gameObject.name != "Wizard Goose(Clone)")
        {
            if (canMove)
            {
                rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
            }
            else
            {
                StopMoving();
            }
        }
    }

    private void Update()
    {
        if (!GameManager.i.paused)
        {
            if (SequenceManager.i.inputEnabled && killable && Input.anyKeyDown)
            {
                CheckForSequence();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.name != "Wizard Goose(Clone)")
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameManager.i.beingKill = true;
                SequenceManager.i.inputEnabled = false;
                GameManager.i.gameOver = true;
                EnemyManager.i.spawn = false;
                GameManager.i.PauseWizardGoose();
                GameManager.i.PauseBackground();

                foreach (Enemy enemy in SequenceManager.i.enemies)
                {
                    enemy.canMove = false;
                    if (enemy.animator != null && enemy.sequence.Count > 0)
                    {
                        enemy.animator.speed = 0f;
                    }
                }
                canMove = false;
                animator.speed = 0.35f;
                GameManager.i.player.animator.Play("Idle");
                animator.Play("Attack");
            }
        }
        if (other.tag == "Killable")
        {
            killable = true;
        }
        else if (other.tag == "CantKill")
        {
            killable = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Stop"))
        {
            animator.CrossFade("Finish Hit", 0);
        }
    }

    public void SetRandomSequence()
    {
        System.Random rnd1 = new System.Random();
        sequenceSize = rnd1.Next(minSequence, maxSequence + 1);

        for (int i = 0; i < sequenceSize; i++)
        {
            System.Random rnd2 = new System.Random();
            int randomDirection = rnd2.Next(1, 5);

            if (randomDirection == 1)
            {
                sequence.Add(KeyCode.UpArrow);
            }
            else if (randomDirection == 2)
            {
                sequence.Add(KeyCode.DownArrow);
            }
            else if (randomDirection == 3)
            {
                sequence.Add(KeyCode.LeftArrow);
            }
            else if (randomDirection == 4)
            {
                sequence.Add(KeyCode.RightArrow);
            }
        }
    }

    private void StopMoving()
    {
        rb.velocity = Vector3.zero;
    }

    private void CheckForSequence()
    {
        KeyCode pressedKey = GetKeyCode();
        if (pressedKey != KeyCode.None && sequence.Count != 0)
        {
            if (!contado)
            {
                SequenceManager.i.cantidadEnemigos++;
                contado = true;
            }

            if (pressedKey == sequence[0])
            {
                uiSequence.DestroyFirstCommand();
                sequence.RemoveAt(0);

                if (sequence.Count == 0)
                {
                    if (gameObject.name == "Evil Wizard(Clone)")
                    {
                        phase++;
                        if (phase != 3)
                        {
                            canMove = false;
                            col.isTrigger = false;
                            GameManager.i.player.animator.Play("Attack 1");
                            animator.Play("Take Hit");
                            minSequence++;
                            maxSequence++;
                        }
                        else
                        {
                            SequenceManager.i.cantidadEnemigos--;
                            SequenceManager.i.flagKeyPressed = false;
                            Invoke("DestroyEnemy", 0.075f);
                        }
                    }
                    else if (gameObject.name != "Wizard Goose(Clone)")
                    {
                        SequenceManager.i.cantidadEnemigos--;
                        SequenceManager.i.flagKeyPressed = false;
                        GameManager.i.player.animator.Play("Attack 1");
                        Invoke("DestroyEnemy", 0.075f);
                    }
                }
            }
            else
            {
                SequenceManager.i.comboActual--;
            }
        }
    }

    public void NewPhase()
    {
        SetRandomSequence();
        foreach (KeyCode key in sequence)
        {
            uiSequence.CreateUI(key);
        }
    }

    private KeyCode GetKeyCode()
    {
        foreach (KeyCode tecla in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(tecla) &&
               (tecla == KeyCode.UpArrow || tecla == KeyCode.DownArrow || tecla == KeyCode.LeftArrow || tecla == KeyCode.RightArrow))
            {
                return tecla;
            }
        }
        return KeyCode.None;
    }

    public void Die()
    {
        Invoke("DestroyEnemy", 0.075f);
    }

    public void DestroyEnemy()
    {
        if (SequenceManager.i.enemies.Count > 0)
        {
            killable = false;
            canMove = false;
            SequenceManager.i.enemies.RemoveAt(0);
            animator.Play("Die");
            col.enabled = false;
        }
    }

    public void SpawnEggBomb()
    {
        eggBombInstance = Instantiate(GameManager.i.eggBomb, animator.transform.position + new Vector3(0, 0f, 0), Quaternion.identity);
        eggBombInstance.transform.parent = transform;
    }
}
