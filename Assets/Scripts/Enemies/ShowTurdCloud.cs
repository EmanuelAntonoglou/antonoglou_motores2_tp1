using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTurdCloud : MonoBehaviour
{
    [SerializeField] private MoveTurdCloud moveTurdCloud;
    [SerializeField] private SpriteRenderer sprRTurdCloud;
    [NonSerialized] private Enemy wizardGoose;

    private void Awake()
    {
        wizardGoose = transform.parent.GetComponent<Enemy>();
    }

    public void ActivateTurdCloud()
    {
        sprRTurdCloud.enabled = true;
        moveTurdCloud.enabled = true;
    }

    public void DesactivateTurdCloud()
    {
        sprRTurdCloud.enabled = false;
        moveTurdCloud.enabled = false;
    }

    public void StopBeingKillable()
    {
        wizardGoose.killable = false;
    }

    public void FinishedAnimation()
    {
        DesactivateTurdCloud();

        if (wizardGoose.sequence.Count != 0)
        {
            Destroy(wizardGoose.gameObject);
        }
        else if (wizardGoose.sequence.Count == 0)
        {
            GameManager.i.StartWizardGooseAttack();
        }
    }
}
